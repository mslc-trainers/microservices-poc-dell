package com.dell.training.ddd.model;

public class Customer {


    private int id;
    private String name;


    public Customer() {

    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer(int id, String name) {

        this.id = id;
        this.name = name;

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
