package com.dell.training.ddd.restcontrollers;


import com.dell.training.ddd.model.Customer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(path = "/customers")
public class CustomerRestController {


    @GetMapping(path = "/{id}")
    public ResponseEntity<Customer> handleGetCustomerbyId() {


        return new ResponseEntity<>(new Customer(101, "Dell"), HttpStatus.OK);

    }

    @GetMapping()
    public ResponseEntity<List<Customer>> handleGetAllCustomers() {


        List<Customer> allCustomers = Arrays.asList(new Customer(101, "Dell"),
                new Customer(102, "IBM"));


        return new ResponseEntity<>(allCustomers, HttpStatus.OK);

    }

}
