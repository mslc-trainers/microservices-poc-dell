package com.dell.training.ddd.dependencies;


import com.dell.training.ddd.model.Customer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient("customer-service")
public interface CustomerService {


    @GetMapping("/customers")
    public List<Customer> getCustomers();

    @GetMapping("/customers/{id}")
    public Customer getCustomer(@PathVariable("id") int id);
}
