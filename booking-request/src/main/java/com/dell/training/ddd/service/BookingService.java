package com.dell.training.ddd.service;

import com.dell.training.ddd.dependencies.CustomerService;
import com.dell.training.ddd.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import reactor.core.publisher.Flux;

import java.util.List;

@Service
public class BookingService {


    @Autowired
    private CustomerService customerService;

    @Autowired
    RestTemplate restTemplate;

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }


    public void createNewBooking() {

//
//        String uri = "http://CUSTOMER-SERVICE/customers";


//        ResponseEntity<List<Customer>> response = restTemplate
//                .exchange(uri,
//                        HttpMethod.GET,
//                        null,
//                        new ParameterizedTypeReference<List<Customer>>() {
//                        });


//        List<Customer> customers = response.getBody();

        List<Customer> customers = customerService.getCustomers();
        for (Customer customer : customers) {
            System.out.println("Using Feign : " + customer);
        }

        System.out.println("---------");

        System.out.println(customerService.getCustomer(202));


    }
}




