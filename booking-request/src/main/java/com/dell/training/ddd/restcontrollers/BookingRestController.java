package com.dell.training.ddd.restcontrollers;


import com.dell.training.ddd.service.BookingService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@RestController
public class BookingRestController {


    @Autowired
    private BookingService bookingService;


    @HystrixCommand(fallbackMethod = "defaultBookings")
    @PostMapping(path = "/bookings")
    public ResponseEntity<Map<String, String>> handlePostBooking() {


        bookingService.createNewBooking();

        Map<String, String> status = new HashMap<>();
        status.put("status", "OK");

        return new ResponseEntity<>(status, HttpStatus.OK);

    }

    private ResponseEntity<Map<String, String>> defaultBooking() {
        Map<String, String> status = new HashMap<>();
        status.put("status", "Default Values");

        return new ResponseEntity<>(status, HttpStatus.OK);

    }
}
